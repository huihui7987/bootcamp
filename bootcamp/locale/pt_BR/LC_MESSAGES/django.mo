��    >        S   �      H     I     Z     h     q     z     �     �     �     �     �     �  )         *  '   D     l     r     z     �     �     �     �  .   �     �     �     �                          (     0     D     [     d     l     y     ~     �     �     �     �     �  	   �     �  
   �     �     �     	            !     
   <     G     N     a     j     }     �     �     �  	   �  @  �     
     )
  
   <
     G
     O
     X
     s
     �
     �
     �
  -   �
  0   �
       0   *     [     b     n     w  	   �     �     �  5   �  	   �     �               "     )     7     <     A     ]     z     �     �     �     �     �      �     �     �     �  	   �                    +     ?     H     L     Q     q     ~     �     �     �     �  1   �          $     +     :   2      4   ,   <       '                           /           "         6   %                         >                 7   1      0             
                     5      *             $         	      &   8       9   (   3      ;   #   +   .   )                           -      !       =    Account Settings All Questions Answered Articles Ask Question Be the first one to comment Be the first one to publish Cancel Change Password Change Picture Click to accept the answer Click to down vote; click again to toggle Click to remove this feed Click to up vote; click again to toggle Close Comment Compose Compose a new post Content Create an account Crop Picture Crop the profile picture and then click on the Drafts Edit Edit Profile Last Feeds by Like Location Log out Network No draft to display No question to display Password Picture Popular Tags Post Post Your Answer Post Your Question Press Ctrl + P to compose Profile Publish Question Questions Save Save Draft Save Picture Save changes See all Tag Tags There is no published article yet Unanswered Unlike Upload new picture Username Write a comment... You have no notification You have no unread notification answered button new posts Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-05 22:50-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Configuração da conta Todas as perguntas Respondida Artigos Pergunte Seja o primeiro a comentar Seja o primeiro a publicar Cancelar Trocar senha Trocar foto Clique caso não queira aceitar essa resposta Clique para votar; clique novamente para alterar Clique para remover o feed Clique para votar; clique novamente para alterar Fechar Comentário Escrever Escrever uma nova postagem Conteúdo Criar uma conta Recortar foto Recorte a foto do seu perfil, e em seguida, clique no Rascunhos Editar Editar Perfil Últimos Feeds por Gostei Localização Sair Rede Nenhum rascunho para exibir Nenhuma pergunta para exibir Senha Foto Tags populares Postagem Postar sua resposta Poste sua pergunta Pressione Ctrl + P para escrever Perfil Publicar Pergunta Perguntas Salvar Salvar rascunho Salvar foto Salvar alterações Ver tudo Tag Tags Não há artigo publicado ainda Sem resposta Não gostei Carregar nova foto Nome de usuário Escrever um comentário Você não possui notificação Você não possui nenhuma notificação não lida respondidas botão novas postagens 